    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyBU8QZxhZdWmlGC2nmiVDtgg8lID_VpgiQ",
        authDomain: "f-app-test.firebaseapp.com",
        databaseURL: "https://f-app-test.firebaseio.com",
        storageBucket: "f-app-test.appspot.com",
        messagingSenderId: "804766313134"
    };
    firebase.initializeApp(config);

    /**
     * Handles the sign in button press.
     */
    function toggleSignIn() {
        //alert();
        if (firebase.auth().currentUser) {
            // [START signout]
            firebase.auth().signOut();
            // [END signout]
            console.log("sign out");
        } else {
            var email = document.getElementById('email').value;
            var password = document.getElementById('password').value;
            
            console.log("signing in");
            if (email.length < 6) {
                //alert('Please enter an email address.');
                document.getElementById('callback-message').innerHTML = 'Please enter an email address.';
                return;
            }
            if (password.length < 6) {
                //alert('Please enter a password.');
                document.getElementById('callback-message').innerHTML = 'Please enter a password.';
                return;
            }
            
            console.log("signing in - > email pass length checked");
            // Sign in with email and pass.
            // [START authwithemail]
            firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                console.log("signing in - > error");
                // [START_EXCLUDE]
                if (errorCode === 'auth/wrong-password') {
                    //alert('Wrong password.');
                    document.getElementById('callback-message').innerHTML = 'Wrong password.';

                } else {
                    //alert(errorMessage);
                    document.getElementById('callback-message').innerHTML = errorMessage;
                }
                console.log(error);

                //document.getElementById('quickstart-sign-in').disabled = false;
                // [END_EXCLUDE]
            });
            // [END authwithemail]
        }
        //document.getElementById('quickstart-sign-in').disabled = true;
    }
    /**
     * Handles the sign up button press.
     */
    function handleSignUp() {
        var email = document.getElementById('email').value;
        var password = document.getElementById('password').value;
        var passwordmatch = document.getElementById('password-match').value;
        if (email.length < 6) {
            //alert('Please enter an email address.');
            document.getElementById('callback-message').innerHTML = 'Please enter an email address.';

            return;
        }
        if (password.length < 6) {
            //alert('Please enter a password.');
            document.getElementById('callback-message').innerHTML = 'Password is too short.';


            return;
        }
        if (password != passwordmatch) {
            //alert('Please enter a password.');
            document.getElementById('callback-message').innerHTML = 'Please enter same password in both fields.';
            return;
        }
        // Sign in with email and pass.
        // [START createwithemail]
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // [START_EXCLUDE]
            if (errorCode == 'auth/weak-password') {
                //alert('The password is too weak.');
                document.getElementById('callback-message').innerHTML = 'The password is too weak.';
            } else {
                //alert(errorMessage);
                document.getElementById('callback-message').innerHTML = errorMessage;
            }
            console.log(error);
        // [END_EXCLUDE]
        })
        // [END createwithemail]
    }
    /**
     * Sends an email verification to the user.
     */
    function sendEmailVerification() {
        // [START sendemailverification]
        firebase.auth().currentUser.sendEmailVerification().then(function() {
            // Email Verification sent!
            // [START_EXCLUDE]
            //alert('Email Verification Sent!');
            document.getElementById('callback-message').innerHTML = 'Email Verification Sent!';
            // [END_EXCLUDE]
        });
        // [END sendemailverification]
    }
    function sendPasswordReset() {
        var email = document.getElementById('email').value;
        // [START sendpasswordemail]
        firebase.auth().sendPasswordResetEmail(email).then(function() {
            // Password Reset Email Sent!
            // [START_EXCLUDE]
            //alert('Password Reset Email Sent!');
            // [END_EXCLUDE]
        }).catch(function(error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // [START_EXCLUDE]
            if (errorCode == 'auth/invalid-email') {
                alert(errorMessage);
            } else if (errorCode == 'auth/user-not-found') {
                alert(errorMessage);
            }
            console.log(error);
            // [END_EXCLUDE]
        });
        // [END sendpasswordemail];
    }
    /**
     * initApp handles setting up UI event listeners and registering Firebase auth listeners:
     *  - firebase.auth().onAuthStateChanged: This listener is called when the user is signed in or
     *    out, and that is where we update the UI.
     */
    function initApp() {
        // Listening for auth state changes.
        // [START authstatelistener]
        console.log("initApp");
        firebase.auth().onAuthStateChanged(function(user) {
            // [START_EXCLUDE silent]
            //document.getElementById('quickstart-verify-email').disabled = true;
            console.log("update it " + user);
            // [END_EXCLUDE]
            if (user) {
                // User is signed in.
                var displayName = user.displayName;
                var email = user.email;
                var emailVerified = user.emailVerified;
                var photoURL = user.photoURL;
                var isAnonymous = user.isAnonymous;
                var uid = user.uid;
                var providerData = user.providerData;

                // [START_EXCLUDE silent]
                // document.getElementById('quickstart-sign-in-status').textContent = 'Signed in';
                // document.getElementById('quickstart-sign-in').textContent = 'Sign out';
                // document.getElementById('quickstart-account-details').textContent = JSON.stringify(user, null, '  ');

                // if (!emailVerified) {
                //     // alert("Verify your email.");
                //     document.getElementById('callback-message').innerHTML = "Go to your email account and verify";
                //     // document.getElementById('verification-send').style.display = "inline";
                // }else{
                    window.location.href = "pageapp-calendar.html";
                // }
            // [END_EXCLUDE]
            } else {
                //window.location.href = "index.html";
                // User is signed out.
                // [START_EXCLUDE silent]
                //document.getElementById('quickstart-sign-in-status').textContent = 'Signed out';
                //document.getElementById('quickstart-sign-in').textContent = 'Sign in';
                //document.getElementById('quickstart-account-details').textContent = 'null';
                // [END_EXCLUDE]
            }
            // [START_EXCLUDE silent]
            // document.getElementById('quickstart-sign-in').disabled = false;
            // [END_EXCLUDE]
        });
        // [END authstatelistener]
        //document.getElementById('signin-btn').addEventListener('click', toggleSignIn, false);
        document.getElementById('signup-btn').addEventListener('click', handleSignUp, false);
        //document.getElementById('logout-btn').addEventListener('click', toggleSignIn, false);

        //document.getElementById('verification-send').addEventListener('click', sendEmailVerification, false);
        //document.getElementById('quickstart-password-reset').addEventListener('click', sendPasswordReset, false);
    }