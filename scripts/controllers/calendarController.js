module.controller('mainHeaderCtrl', ['$rootScope','$scope', function($rootScope,$scope){

	//$scope.somedata = 000;
    var Mydata = null;

    $scope.somedata = [];

//    console.log(0030);

    $scope.taskEditShowAll = true;
    $scope.taskEditShow1 = false;
    $scope.taskEditShow2 = false;
    $scope.taskEditShow3 = false;
    $scope.taskEditShow4 = false;

    $scope.TaskToggleAll = function(){
        $scope.taskEditShowAll = true;
        $scope.taskEditShow1 = false;
        $scope.taskEditShow2 = false;
        $scope.taskEditShow3 = false;
        $scope.taskEditShow4 = false;
    }

    $scope.TaskToggle1 = function(){
        $scope.taskEditShowAll = false;
        $scope.taskEditShow1 = true;
        $scope.taskEditShow2 = false;
        $scope.taskEditShow3 = false;
        $scope.taskEditShow4 = false;
    }
    $scope.TaskToggle2 = function(){
        $scope.taskEditShowAll = false;
        $scope.taskEditShow1 = false;
        $scope.taskEditShow2 = true;
        $scope.taskEditShow3 = false;
        $scope.taskEditShow4 = false;
    }
    $scope.TaskToggle3 = function(){
        $scope.taskEditShowAll = false;
        $scope.taskEditShow1 = false;
        $scope.taskEditShow2 = false;
        $scope.taskEditShow3 = true;
        $scope.taskEditShow4 = false;
    }
    $scope.TaskToggle4 = function(){
        $scope.taskEditShowAll = false;
        $scope.taskEditShow1 = false;
        $scope.taskEditShow2 = false;
        $scope.taskEditShow3 = false;
        $scope.taskEditShow4 = true;
    }


	$scope.fetchData = function(){
        console.log("----");
        $scope.somedata = [];
        
        $scope.EventDate = $('#EventDate').val();
        console.log($scope.EventDate);
        if($scope.EventDate == null || $scope.EventDate == '' || $scope.EventDate == undefined){
            try{
                document.getElementById('fieldMessage').innerHTML = "Enter Date";
            }catch(err){
                
            }

        }else{
            if($scope.taskEditShowAll){
                var starCountRef = firebase.database().ref('events/team1').orderByChild('date').equalTo($scope.EventDate);
                starCountRef.on('child_added', function(data) {
                    $scope.somedata.push(data.val());
                    console.log(data.val());
                    
                    // var author = data.val().author || 'Anonymous';
                    // var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
                    // containerElement.insertBefore(
                    //     createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic),
                    //     containerElement.firstChild);
                });
                var starCountRef = firebase.database().ref('events/team2').orderByChild('date').equalTo($scope.EventDate);
                starCountRef.on('child_added', function(data) {
                    $scope.somedata.push(data.val());
                    console.log(data.val());
                    
                    // var author = data.val().author || 'Anonymous';
                    // var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
                    // containerElement.insertBefore(
                    //     createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic),
                    //     containerElement.firstChild);
                });
                var starCountRef = firebase.database().ref('events/team3').orderByChild('date').equalTo($scope.EventDate);
                starCountRef.on('child_added', function(data) {
                    $scope.somedata.push(data.val());
                    console.log(data.val());
                    
                    // var author = data.val().author || 'Anonymous';
                    // var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
                    // containerElement.insertBefore(
                    //     createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic),
                    //     containerElement.firstChild);
                });
                var starCountRef = firebase.database().ref('events/team4').orderByChild('date').equalTo($scope.EventDate);
                starCountRef.on('child_added', function(data) {
                    $scope.somedata.push(data.val());
                    console.log(data.val());
                    
                    // var author = data.val().author || 'Anonymous';
                    // var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
                    // containerElement.insertBefore(
                    //     createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic),
                    //     containerElement.firstChild);
                });

            }else if($scope.taskEditShow1){
                var starCountRef = firebase.database().ref('events/team1').orderByChild('date').equalTo($scope.EventDate);
                starCountRef.on('child_added', function(data) {
                    $scope.somedata.push(data.val());
                    console.log(data.val());
                    
                    // var author = data.val().author || 'Anonymous';
                    // var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
                    // containerElement.insertBefore(
                    //     createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic),
                    //     containerElement.firstChild);
                });
            }else if($scope.taskEditShow2){
                var starCountRef = firebase.database().ref('events/team2').orderByChild('date').equalTo($scope.EventDate);
                starCountRef.on('child_added', function(data) {
                    $scope.somedata.push(data.val());
                    console.log(data.val());
                    
                    // var author = data.val().author || 'Anonymous';
                    // var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
                    // containerElement.insertBefore(
                    //     createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic),
                    //     containerElement.firstChild);
                });
            }else if($scope.taskEditShow3){
                var starCountRef = firebase.database().ref('events/team3').orderByChild('date').equalTo($scope.EventDate);
                starCountRef.on('child_added', function(data) {
                    $scope.somedata.push(data.val());
                    console.log(data.val());
                    
                    // var author = data.val().author || 'Anonymous';
                    // var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
                    // containerElement.insertBefore(
                    //     createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic),
                    //     containerElement.firstChild);
                });
            }else if($scope.taskEditShow4){
                var starCountRef = firebase.database().ref('events/team4').orderByChild('date').equalTo($scope.EventDate);
                starCountRef.on('child_added', function(data) {
                    $scope.somedata.push(data.val());
                    console.log(data.val());
                    
                    // var author = data.val().author || 'Anonymous';
                    // var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
                    // containerElement.insertBefore(
                    //     createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic),
                    //     containerElement.firstChild);
                });
            }

            
        }
        


        

        // starCountRef.on('child_added', function(snapshot, prevChildKey) {

        //     var someObjStr = JSON.stringify(snapshot.val());

        //     $scope.somedata = Object.keys(snapshot.val()).length ;

        //     Mydata = Object.keys(snapshot.val());

        //     console.log(Object.keys(snapshot.val()));

        //     $scope.ekfunction(Object.keys(snapshot.val()));

        //     //updateStarCount(postElement, snapshot.val());
        // });

        //$scope.somedata = Mydata ;
        //onsole.log(Mydata);
    }

    $scope.eknumber = 0;

    $scope.toggle = function(){

        if($scope.eknumber==0){

            $scope.eknumber =1;

        }else{

            $scope.eknumber =0;

        }
    }

    $scope.ekfunction = function(num){

        $scope.somedata = num;
        console.log(num);

    }
//    console.log($scope.somedata);



}]);