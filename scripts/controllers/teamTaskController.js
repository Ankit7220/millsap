module.controller('mainHeaderCtrl', ['$rootScope','$scope', function($rootScope,$scope){

	//$scope.somedata = 000;
    var Mydata = null;

    $rootScope.TeamTask1 = "Loading tasks";
    $rootScope.TeamTask2 = "Loading tasks";
    $rootScope.TeamTask3 = "Loading tasks";
    $rootScope.TeamTask4 = "Loading tasks";

    var task1 = "";
    var task2 = "";
    var task3 = "";
    var task4 = "";

    $scope.taskEditShow1 = false;
    $scope.taskEditShow2 = false;
    $scope.taskEditShow3 = false;
    $scope.taskEditShow4 = false;

    $scope.TaskToggle1 = function(){
        $scope.taskEditShow1 = !$scope.taskEditShow1;
    }
    $scope.TaskToggle2 = function(){
        $scope.taskEditShow2 = !$scope.taskEditShow2;
    }
    $scope.TaskToggle3 = function(){
        $scope.taskEditShow3 = !$scope.taskEditShow3;
    }
    $scope.TaskToggle4 = function(){
        $scope.taskEditShow4 = !$scope.taskEditShow4;
    }


    $rootScope.setTask1 = function(){
        $rootScope.TeamTask1 = task1;
    }
    $rootScope.setTask2 = function(){
        $rootScope.TeamTask2 = task2;
    }
    $rootScope.setTask3 = function(){
        $rootScope.TeamTask3 = task3;
    }
    $rootScope.setTask4 = function(){
        $rootScope.TeamTask4 = task4;
    }

    $scope.writeTaskData1 = function () {
        task1 = document.getElementById('taskEdit1').value;
        $rootScope.TeamTask1 = task1;
        firebase.database().ref().update({
            'temas/TeamTask1' : task1
        });
    }
    $scope.writeTaskData2 = function () {
        task2 = document.getElementById('taskEdit2').value;
        $rootScope.TeamTask2 = task2;
        firebase.database().ref().update({
            'temas/TeamTask2' : task2
        });
    }
    $scope.writeTaskData3 = function () {
        task3 = document.getElementById('taskEdit3').value;
        $rootScope.TeamTask3 = task3;
        firebase.database().ref().update({
            'temas/TeamTask3' : task3
        });
    }
    $scope.writeTaskData4 = function () {
        task4 = document.getElementById('taskEdit4').value;
        $rootScope.TeamTask4 = task4;
        firebase.database().ref().update({
            'temas/TeamTask4' : task4
        });
    }


    
	$scope.fetchData = function(){
        console.log("----");
        
        
        {

            var starCountRef = firebase.database().ref('temas/TeamTask1');
            starCountRef.on('value', function(data) {
                task1 = (data.val());
                $rootScope.setTask1();
                console.log(data.val());
                
            });
            var starCountRef = firebase.database().ref('temas/TeamTask2');
            starCountRef.on('value', function(data) {
                task2 = (data.val());
                $rootScope.setTask2();
                console.log(data.val());
                
            });
            var starCountRef = firebase.database().ref('temas/TeamTask3');
            starCountRef.on('value', function(data) {
                task3 = (data.val());
                $rootScope.setTask3();
                console.log(data.val());
                
            });
            var starCountRef = firebase.database().ref('temas/TeamTask4');
            starCountRef.on('value', function(data) {
                task4 = (data.val());
                $rootScope.setTask4();
                console.log(data.val());
                
            });
        }
    }

    $scope.fetchData();


}]);