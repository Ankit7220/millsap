var global_username = '';
var global_recipient = '';
//start an instance of sinchClient
$('button#createUser').on('click', function(event) {
    //don't submit the form (preventDefault)
    //disable buttons while logging in user  
    //get username & password from form
    //register new user with sinchClient
    //success: login user
    //success: prompt to pick recipient
    //fail: display error message
    //fail: display error message
});
$('button#loginUser').on('click', function(event) {
    //don't submit the form (preventDefault)
    //disable buttons while logging in user  
    //get username & password from form
    //login user
    //success: prompt to pick recipient
    //fail: display error message
});



sinchClient = new SinchClient({
    applicationKey: '0ac6e456-0154-41cc-9518-c5e35e14737c',
    capabilities: {messaging: true},
});
event.preventDefault();
$('button#createUser').attr('disabled', true);
$('button#loginUser').attr('disabled', true);

var username = $('input#username').val();
var password = $('input#password').val();

var loginObject = {username: username, password: password};
sinchClient.newUser(loginObject, function(ticket) {
    //On success, login user   
}).fail(handleError);

var handleError = function(error) {}

sinchClient.start({username: username, password: password}, function() {
    global_username = username;
    console.log("Logged in!");
}).fail(handleError);

$('button#createUser').attr('disabled', false);
$('button#loginUser').attr('disabled', false);
$('div.error').text(error.message);

var clearError = function() {
    $('div.error').text("");
}